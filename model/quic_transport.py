#!/usr/bin/python3

import re
import sys


lines = sys.stdin.readlines()
LEMMA = sys.argv[1]

# INPUT:
# - lines contain a list of "%i:goal" where "%i" is the index of the goal
# - lemma contain the name of the lemma under scrutiny
# OUTPUT:
# - (on stdout) a list of ordered index separated by EOL

MAX_PRIO = 100
rank = [
    [] for _ in range(MAX_PRIO)
]  # list of list of goals, main list is ordered by priority


def rank_goal(rank_list: list[list[int]], line: str, goals_regex: list[str]):
    num = line.split(":")[0]
    rank = len(rank_list) - 1
    found = False
    for rgx in goals_regex:
        if re.match(rgx, line):
            found = True
            break
        rank -= 1
    if rank < 0 or not (found):
        rank = 0
    rank_list[rank].append(num)


# SOURCES AND REUSE LEMMAS

if LEMMA == "REUSE_PacketMustHaveBeenSentBeforeRetransmit":
    for line in lines:
        rank_goal(
            rank,
            line,
            [
                r".*\(∀ session_id_c S C stream_frame #i\..*\(Retransmit_Frame\( session_id_c, C, S, stream_frame.*",
                r".*!Sk\( \$C, \$S, C_secret\.\d+, C_key\.\d+, C_IV\.\d+, S_secret\.\d+, S_key\.\d+, .*",
                r".*Client_packet_waiting_ACK\( ~session_id_c, \$C, \$S, stream_frame,.*",
            ],
        )


elif LEMMA == "REUSE_ACK_sent_by_server":
    for line in lines:
        rank_goal(
            rank,
            line,
            [
                r".*!Sk\( \$C, \$S, C_secret\.\d+, C_key\.\d+, C_IV\.\d+, S_secret\.\d+, S_key\.\d+.*",
                r".*!KU\( aeadenc\(<ack_frame_type,.*",
                r".*!KU\( aeadenc\(<stop_sending_frame_type,.*",
                r".*!Client_Key_state\(.*",
                r".*!KU\( ~S_key(\.\d+)? \).*",
                r".*!KU\( ~C_key(\.\d+)? \).*",
                r".*!KU\( ~error_code \).*",
                r".*!KU\( ~c_connection_id \).*",
                r".*!KU\( ~s_connection_id \).*",
                r".*!Client_uni\(.*",
            ],
        )

elif LEMMA == "Received_payload_secrecy" or LEMMA == "Injective_agreement_frame":
    for line in lines:
        rank_goal(
            rank,
            line,
            [
                r".*!KU\( aeadenc\(<stop_sending_frame_type,.*",
                r".*!KU\( ~S_key(\.\d+)? \).*",
                r".*!KU\( ~C_key(\.\d+)? \).*",
                r".*!Client_Key_state\(.*",
                r".*!Server_Key_state\(.*",
                r".*!KU\( aeadenc\(<stream_frame_type, fin_bit, *",
                r".*!Sk\( \$C, \$S, C_key, C_IV, S_key, S_IV \).*",
                r".*!KU\( ~data \).*",
                r".*!KU\( ~error_code \).*",
                r".*!KU\( aeadenc\(.*",
                r".*!Client_uni\(.*",
            ],
        )

elif LEMMA == "Sent_payload_secrecy":
    for line in lines:
        rank_goal(
            rank,
            line,
            [
                r".*!KU\( aeadenc\(<stop_sending_frame_type,.*",
                r".*!KU\( ~S_key(\.\d+)? \).*",
                r".*!KU\( ~C_key(\.\d+)? \).*",
                r".*!KU\( ~data \).*",
                r".*!KU\( ~error_code \).*",
                r".*!Client_Key_state\(.*",
                r".*!Server_Key_state\(.*",
                r".*!KU\( aeadenc\(<stream_frame_type, fin_bit, *",
                r".*!Sk\( \$C, \$S, C_key, C_IV, S_key, S_IV \).*",
                r".*!KU\( ~data \).*",
                r".*!KU\( aeadenc\(.*",
                r".*!Client_uni\(.*",
            ],
        )

else:
    for line in lines:
        num = line.split(":")[0]
        rank_goal(
            rank,
            line,
            [
                r".*!KU\( aeadenc\(<stop_sending_frame_type,.*",
                r".*!KU\( ~S_key(\.\d+)? \).*",
                r".*!KU\( ~C_key(\.\d+)? \).*",
                r".*!Client_Key_state\(.*",
                r".*!Server_Key_state\(.*",
                r".*!Sk\( \$C, \$S, C_key, C_IV, S_key, S_IV \).*",
                r".*!KU\( ~error_code \).*",
                r".*!Client_uni\(.*",
            ],
        )

# else:
#    sys.exit(0)


# Ordering all goals by ranking (higher first)
for listGoals in reversed(rank):
    for goal in listGoals:
        print(goal)
