# Model description

## Facts


- `!Client_connection_state(~session_id_c, $C, $S, %packet_number, %client_uni, client_bidi, server_uni, server_bidi)`  
  **Store the general informations about the connection**
  - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `%packet_number`: the actual number of packet sent during the session
  - `%client_uni`: the actual number of unidirectional streams initiated by the client that have been created during the session
  - `client_bidi`: the actual number of bidirectional streams initiated by the client that have been created during the session
  - `server_uni`: the actual number of unidirectional streams initiated by the server that have been created during the session
  - `server_bidi`: the actual number of bidirectional streams initiated by the server that have been created during the session

- `!Client_connection_id_server(~session_id_c, $C, $S, ~s_connection_id, %s_cid_seq_num, latest_s_cid_bit)`  
  **Store a connection id belonging to the server on the client side**
   - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `~s_connection_id`: a fresh value representing a random connection id chosen by the server
  - `%s_cid_seq_num`: the sequence number of the connection id, increased by 1 each time a new cid is generated by the server in the session
  - `latest_s_cid_bit`: a bit which value is `'1'` if the fact stores the latest server connection id known to the client, else `'0'`

- `!Client_connection_id_client(~session_id_c, $C, $S, ~c_connection_id, %c_cid_seq_num, latest_c_cid_bit)`  
  **Store a connection id belonging to the client on the client side**
  - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `~s_connection_id`: a fresh value representing a random connection id chosen by the client
  - `%c_cid_seq_num`: the sequence number of the connection id, increased by 1 each time a new cid is generated by the client in the session
  - `latest_c_cid_bit`: a bit which value is `'1'` if the fact stores the latest client connection id generated by the client, else `'0'`

- `!Client_Key_state(~session_id_c, C_secret, C_key, C_IV, S_secret, S_key, S_IV, phase_bit, main_keys_bit)`  
  **Store the cryptographic material and keys used during a session**
  - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `C_secret`: a secret from which the client keys and IV are derived
  - `C_keys`: the keys used by the client to encrypt the payloads sent by the client
  - `C_IV`: an initialisation vector used to calculate nonces to encrypt the data sent by the client
  - `S_secret`: a secret from which the server keys and IV are derived
  - `S_keys`: the keys used by the server to encrypt the payloads sent by the server
  - `S_IV`: an initialisation vector used to calculate nonces the encrypt the data sent by the server
  - `phase_bit`: the value of the phase bit in the headers of the packets using those keys to encrypt the paylaod
  - `main_keys_bit`: a bit which value is `'1'` if the fact stores the latest cryptographic material of the session, else `'0'`

- `!Client_uni(~session_id_c, $C, $S, stream_id, %offset)`  
  **Store the informations about an unidirectional stream initiated by the client**
  - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `stream_id`: the id of the stream
  - `%offset`: the current offset of the data in the stream

- `Client_packet_waiting_ACK(~session_id_c, $C, $S, frame, %packet_number)`  
  **Store the informations about a sent packet waiting for an ACK**
  - `~session_id_c` : a fresh value only known by the client to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `frame`: a copy of the frame sent
  - `%packet_number`: a copy of the packet number where the frame was sent

- `!Server_connection_state(~session_id_s, $C, $S, %packet_number, client_uni, client_bidi, server_uni, server_bidi)`  
  **Store the general informations about the connection on the server side**
  - `~session_id_s` : a fresh value only known by the server to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `%packet_number`: the actual number of packet sent during the session
  - `client_uni`: the actual number of unidirectional streams initiated by the client that have been created during the session
  - `client_bidi`: the actual number of bidirectional streams initiated by the client that have been created during the session
  - `server_uni`: the actual number of unidirectional streams initiated by the server that have been created during the session
  - `server_bidi`: the actual number of bidirectional streams initiated by the server that have been created during the session

- `!Server_connection_id_server(~session_id_s, $C, $S, ~s_connection_id, %s_cid_seq_num, latest_s_cid_bit)`  
  **Store a connection id belonging to the server on the server side**
  - `~session_id_s` : a fresh value only known by the server to link all the facts associated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `~s_connection_id`: a fresh value representing a random connection id chosen by the server
  - `%s_cid_seq_num`: the sequence number of the connection id, increased by 1 each time a new cid is generated by the server in the session
  - `latest_s_cid_bit`: a bit which value is `'1'` if the fact stores the latest server connection id known to the client, else `'0'`

- `!Server_connection_id_client(~session_id_s, $C, $S, ~c_connection_id, %c_cid_seq_num, latest_c_cid_bit)`  
  **Store a connection id belonging to the client on the server side**
  - `~session_id_s` : a fresh value only known by the server to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `~s_connection_id`: a fresh value representing a random connection id chosen by the client
  - `%c_cid_seq_num`: the sequence number of the connection id, increased by 1 each time a new cid is generated by the client in the session
  - `latest_c_cid_bit`: a bit which value is `'1'` if the fact stores the latest client connection id generated by the client, else `'0'`

- `!Server_Key_state(~session_id_s, C_secret, C_key, C_IV, S_secret, S_key, S_IV, phase_bit, main_keys_bit)`  
  **Store the cryptographic material and keys used during a session**
  - `~session_id_s` : a fresh value only known by the server to link all the facts assiciated to a connection
  - `C_secret`: a secret from which the client keys and IV are derived
  - `C_keys`: the keys used by the client to encrypt the payloads sent by the client
  - `C_IV`: an initialisation vector used to calculate nonces to encrypt the data sent by the client
  - `S_secret`: a secret from which the server keys and IV are derived
  - `S_keys`: the keys used by the server to encrypt the payloads sent by the server
  - `S_IV`: an initialisation vector used to calculate nonces the encrypt the data sent by the server
  - `phase_bit`: the value of the phase bit in the headers of the packets using those keys to encrypt the paylaod
  - `main_keys_bit`: a bit which value is `'1'` if the fact stores the latest cryptographic material of the session, else `'0'`

- `Server_packet_to_be_acknowledged(~session_id_s, $C, $S, %received_packet_number),`  
  **Store the informations about a packet received by a server which has not been acknoledged**
  - `~session_id_s` : a fresh value only known by the server to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `%received_packet_number`: the number of the packet to be acknowledged  

- `Server_Received_stream(~session_id_s, $C, $S, stream_id)`  
  - `~session_id_s` : a fresh value only known by the server to link all the facts assiciated to a connection
  - `$C`: client identity
  - `$S`: server identity
  - `stream_id`: the id of the stream received by a server
