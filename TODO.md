# TODO list QUIC

- [ ] QUIC transport
    - [ ] Renewing connection ID
        - [x] Allow client to initiate a connection id rotation
        - [ ] Allow server to initiate a connection id rotation
    - [ ] Renewing keys
        - [ ] Allow client to renew its keys
        - [ ] Allow server to renew its keys
    - [ ] Server initiated stream
    - [ ] Bidirectional streams
- [ ] QUIC handshake

