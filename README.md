# Modeling QUIC protocol using Tamarin

QUIC RFC: <https://www.rfc-editor.org/rfc/rfc9000.html>

## Tamarin model

### What is implemented

In the current model, a client can:
- Create an unbounded number of session with a server
- Create an unbounded number of streams in a connection
- Send an unbounded number of packets in a connection
- Retransmit a packet
- Close a stream
- Close a stream in an unexpected manner (RESET_STREAM)
- Receive ack from a server
- Renew its connection id

A server can:
- Receive stream frames from a client
- Send ACK for received packets
- Send STOP_SENDING frames to a client to interrupt a stream

### To-do list

[TODO.md](/TODO.md) contains a to-do list to complete the model

### Abstractions

This model abstracts the following aspects of the QUIC protocol:

- There is no congestion control
  | Since Tamarin doesn't model a real network it is pointless to modelise the congestion control mecanism of QUIC
- There is no spin bit
  | The [Latency Spin Bit](https://www.rfc-editor.org/rfc/rfc9000.html#name-latency-spin-bit) is used to enable latency monitoring from observation points in the network path
- There is no length counter for the data in `STREAM_FRAMES`
  | In this model data is represented by a fresh type and it doesn't have a length, so we replace the data length in stream_frame with 'nil' and we simply increment the offset by 1 for each packet
- Packet numbers are not protected
  | The packet number least significant bits are [protected](https://www.rfc-editor.org/rfc/rfc9001.html#name-header-protection) with a key but Tamarin was not adapted to model this kind of behavior, so in this model the packet number is send unprotected
- Each packet can contain only one frame
  | In the QUIC protocol a packet can contain multiple frames in its payload but in this model there could be only one frame per packet. This is due to the fact that modeling an array of frame with an arbitrary size is not possible with Tamarin.

## State diagrams

### Sending streams

![Sending packets](./docs/state_diagrams/QUIC_sending_data.svg)

### Receiving streams

![Receiving packets](./docs/state_diagrams/QUIC_receiving_data.svg)

