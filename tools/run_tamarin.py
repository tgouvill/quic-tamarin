#!python3
"""
This is a CLI tool to run proofs in parallel using the Tamarin Prover

Usage : run this script with `--help`
"""
import multiprocessing
import time
from dataclasses import dataclass
import subprocess
import shlex
import re
from multiprocessing.pool import ThreadPool
from typing import Optional
from enum import Enum
import argparse
from pprint import pprint


class ExplorationMethods(Enum):
    DFS = "DFS"
    BFS = "BFS"


@dataclass
class ProofParams:
    core_number: int
    timeout: int = -1
    exploration_method: ExplorationMethods = ExplorationMethods.DFS
    auto_sources: bool = False


@dataclass
class Lemma:
    """
    Store the information about a lemma proof
    """

    name: str
    type: str
    params: ProofParams
    oracle: None | str = None
    err: bool = False
    status: str = ""
    steps: int = 0
    proof_time: float = -1


def build_command(spthy_file: str, lemma: Lemma) -> str:
    """
    Construct the command to run the proof lemma
    """
    cmd = "tamarin-prover"
    if lemma.params.auto_sources:
        cmd += " --auto-sources"
    if lemma.oracle is not None:
        cmd += f" --heuristic=O --oraclename={lemma.oracle}"
    cmd += f" --prove={lemma.name}"
    cmd += f" --stop-on-trace={lemma.params.exploration_method.value}"
    cmd += f" {spthy_file}"
    cmd += f" +RTS -N{lemma.params.core_number} -RTS"
    return cmd


def run_lemma(spthy_file: str, lemma: Lemma) -> Lemma:
    """
    Run tamarin-prover to prove a lemma and collect the results
    """
    cmd = build_command(spthy_file, lemma)
    print(f"Running proof for {lemma.name}...")
    start_time = time.time()
    retcode, out, _ = call_proc(cmd)
    end_time = time.time()
    lemma.proof_time = end_time - start_time
    if retcode != 0:
        lemma.err = True
        print(f"Finished {lemma.name} with errors")
        return lemma

    reg = re.compile(rf"{lemma.name} \({lemma.type}\): ([a-z]+) \((\d+) steps\)")

    matches = reg.search(out)
    if matches is None:
        raise Exception("Impossible to parse command result")
    lemma.status = matches.group(1)
    lemma.steps = int(matches.group(2))

    print(
        f"Finished {lemma.name} ({lemma.status} in {lemma.steps} steps) in {lemma.proof_time:.2f} seconds !"
    )
    return lemma


def call_proc(cmd: str, timeout: Optional[float] = None) -> tuple[int, str, str]:
    """
    Run a command in a shell and return its output (return code, stdout &
    stderr) as UTF-8 string in a tuple
    """
    with subprocess.Popen(
        shlex.split(cmd),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ) as proc:
        out, err = proc.communicate(timeout=timeout)
        proc.wait()
        return (proc.returncode, out.decode(), err.decode())


def get_lemmas(spthy_file: str, proof_params: ProofParams) -> list[Lemma]:
    """
    Find the names of all the lemmas in a Tamarin theory file
    """
    cmd = f"tamarin-prover {spthy_file} --derivcheck-timeout=0"
    retcode, out, err = call_proc(cmd)
    if retcode != 0:
        raise Exception(f"Error while executing '{cmd}' : '{err}'")
    reg = re.compile(r"([a-zA-Z_]+) \(([a-z\-]+)\): analysis incomplete")
    matches = reg.findall(out)
    lemmas: list[Lemma] = []
    for match in matches:
        lemmas.append(Lemma(name=match[0], type=match[1], params=proof_params))
    return lemmas


if __name__ == "__main__":
    # Parsing CLI arguments
    parser = argparse.ArgumentParser(description="Prove Tamarin lemmas in parallel")
    parser.add_argument(
        "spthy_file", metavar="<spthy file>", type=str, help="the Tamarin theory file"
    )
    parser.add_argument(
        "--cpl",
        dest="core_per_lemma",
        metavar="<core per lemma>",
        default=1,
        type=int,
        help="the number of core per lemma",
    )
    parser.add_argument(
        "--max-cores",
        dest="max_cores",
        metavar="<core number>",
        default=multiprocessing.cpu_count(),
        type=int,
        help="the maximum number of cores to run the proof",
    )
    parser.add_argument(
        "--oracle",
        dest="oracle",
        metavar="<oracle file>",
        default=None,
        type=str,
        help="the oracle file (must be executable)",
    )
    parser.add_argument(
        "--bfs-on-exists-trace",
        dest="bfs_exists_trace",
        action="store_true",
        default=False,
        help="use BFS strategy with exists-trace lemmas",
    )
    parser.add_argument(
        "--auto-sources",
        dest="auto_sources",
        action="store_true",
        default=False,
        help="use --auto-sources with tamarin-prover",
    )
    args = parser.parse_args()
    spthy_file = args.spthy_file
    core_per_lemma = args.core_per_lemma
    total_core_number = max(args.max_cores, core_per_lemma)

    default_proof_params = ProofParams(
        core_number=core_per_lemma, auto_sources=(args.auto_sources is not None)
    )
    lemmas = get_lemmas(spthy_file, default_proof_params)

    pool_thread_number = total_core_number // core_per_lemma

    print(f"Machine core number: {total_core_number}")
    print(f"Number of threads per lemma: {core_per_lemma}")
    print(f"Pool thread number: {pool_thread_number}")
    print(f"Number of lemmas: {len(lemmas)}")
    if args.oracle is not None:
        print(f"Oracle: {args.oracle}")

    print("Lemmas to prove:")
    for lemma in lemmas:
        if args.bfs_exists_trace and lemma.type == "exists-trace":
            lemma.params.exploration_method = ExplorationMethods.BFS
        lemma.oracle = args.oracle
        print(
            f"\t{lemma.name} ({lemma.type}, exploration type: {lemma.params.exploration_method.value})"
        )

    pool = ThreadPool(pool_thread_number)

    results = []

    print("\nStarting proof:")
    for lemma in lemmas[::-1]:
        results.append(pool.apply_async(run_lemma, (spthy_file, lemma)))

    pool.close()
    pool.join()

    print("\nAll proofs done!")
    for result in results:
        print(result.get())
